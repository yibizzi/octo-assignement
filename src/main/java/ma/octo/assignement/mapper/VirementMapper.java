package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.ComptesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VirementMapper {

    @Autowired
    ComptesService comptesService;



    public VirementDto mapVirementToVirementDto(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());

        return virementDto;

    }
    public Virement mapVirementDtoToVirement(VirementDto virementDto) {

        Compte compteEmetteur = this.comptesService.getCompteDetails(String.valueOf(virementDto.getNrCompteEmetteur()));
        Compte compteBeneficiaire = this.comptesService.getCompteDetails(String.valueOf(virementDto.getNrCompteBeneficiaire()));

        Virement virement = new Virement();

        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setDateExecution(virementDto.getDate());
        virement.setMotifVirement(virementDto.getMotif());

        return virement;

    }
}
