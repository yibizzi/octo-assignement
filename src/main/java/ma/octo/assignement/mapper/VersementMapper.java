package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.service.ComptesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersementMapper {
    @Autowired
    ComptesService comptesService;



    public VersementDto mapVersementToVersementDto(Versement versement) {
        VersementDto versementDto = new VersementDto();

        versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());
        versementDto.setMontantVersement(versement.getMontantVersement());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMotif(versement.getMotifVersement());
        versementDto.setNomCompletEmetteur(versement.getNom_prenom_emetteur());

        return versementDto;

    }
    public Versement mapVersementDtoToVersement(VersementDto versementDto) {


        Compte compteBeneficiaire = this.comptesService.getCompteDetails(String.valueOf(versementDto.getNrCompteBeneficiaire()));

        Versement versement = new Versement();

        versement.setNom_prenom_emetteur(versementDto.getNomCompletEmetteur());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setDateExecution(versementDto.getDate());
        versement.setMotifVersement(versementDto.getMotif());

        return versement;

    }

}
