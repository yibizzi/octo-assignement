package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilisateursService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    public List<Utilisateur> getListeDesUtilisateurs() {
        return utilisateurRepository.findAll();
    }

}
