package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.common.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.validation.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementService {

    @Autowired
    VersementRepository versementRepository;


    @Autowired
    ComptesService comptesService;


    @Autowired
    AuditService auditService;


    public List<Versement> getListeDesVersements(){
        return versementRepository.findAll();
    }

    /**
     * Effectuer un versement  dans un compte benificiaire.
     * @param versement
     */
    public void effectuerVersement(Versement versement){

        /**
         * Extraire le compte du bénificiaire + montant
         */
        Compte benificiaire = versement.getCompteBeneficiaire();

        BigDecimal montant = versement.getMontantVersement();

        /**
         * Ajouter le montant au compte de l'utilisateur
         */
        comptesService.addSolde(benificiaire, montant);

        /**
         * Enregistrer le versement dans l'histoire des versements.
         */
        versementRepository.save(versement);

        /**
         * Créer un audit pour l'opération.
         */
        auditService.auditVirement("Versment de Mr(Mme) " + versement.getNom_prenom_emetteur()
                + " pour  " + benificiaire.getNrCompte()
                + " d'un montant de " + versement.getMontantVersement()
                .toString());

    }


}
