package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ComptesService {

    @Autowired
    private CompteRepository compteRepository;

    /**
     * Vérifier si un numéro de compte est valide
     * @param numeroCompte
     * @return
     */
    public boolean isValidAccount(String numeroCompte){
        return compteRepository.findByNrCompte(numeroCompte) != null;
    }

    public Compte getCompteDetails(String numeroCompte){
        return compteRepository.findByNrCompte(numeroCompte);
    }

    public void substractSolde(Compte utilisateur, BigDecimal montant){
        utilisateur.setSolde(utilisateur.getSolde().subtract(montant));
        compteRepository.save(utilisateur);
    }

    public void addSolde(Compte utilisateur, BigDecimal montant){
        utilisateur.setSolde(utilisateur.getSolde().add(montant));
        compteRepository.save(utilisateur);
    }
}
