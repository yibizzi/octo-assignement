package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.common.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VirementService {

    @Autowired
    VirementRepository virementRepository;

    @Autowired
    ComptesService comptesService;

    @Autowired
    AuditService auditService;


    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    /**
     * retourner la liste de tous les virements.
     * @return
     */
    public List<Virement> getListeDesVirements() {
        return virementRepository.findAll();
    }

    /**
     * Effectuer un virement d'un compte emetteur vers un compte benificiaire.
     * @param virement
     */
    public void effectuerVirement(Virement virement) throws SoldeDisponibleInsuffisantException {
        Compte emetteur = virement.getCompteEmetteur();
        Compte benificiaire = virement.getCompteBeneficiaire();

        BigDecimal montant = virement.getMontantVirement();

        if (emetteur.getSolde().intValue() < montant.intValue()) {
            LOGGER.error("Solde insuffisant pour l'utilisateur: " + emetteur.getNrCompte());
            throw new SoldeDisponibleInsuffisantException();
        }

        comptesService.substractSolde(emetteur, montant);
        comptesService.addSolde(benificiaire, montant);

        virementRepository.save(virement);

        auditService.auditVirement("Virement depuis " + emetteur.getNrCompte()
                + " vers " + benificiaire.getNrCompte()
                + " d'un montant de " + virement.getMontantVirement()
                .toString());
    }
}
