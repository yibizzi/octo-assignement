package ma.octo.assignement.exceptions.Virements;

import ma.octo.assignement.exceptions.TransactionException;

public class MontantMaximalDeVirementAtteintException extends TransactionException {
    public MontantMaximalDeVirementAtteintException() {
        super("Montant maximal de virement dépassé");
    }
}
