package ma.octo.assignement.exceptions.Virements;

import ma.octo.assignement.exceptions.TransactionException;

public class MontantMinimalDeVirementNonAtteintException extends TransactionException {
    public MontantMinimalDeVirementNonAtteintException() {
        super("Montant minimal de virement non atteint");
    }
}
