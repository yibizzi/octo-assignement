package ma.octo.assignement.exceptions.Versements;

import ma.octo.assignement.exceptions.TransactionException;

public class MontantMinimalDeVersementNonAtteintException extends TransactionException {
    public MontantMinimalDeVersementNonAtteintException() {
        super("Montant minimal de versement non atteint");
    }
}
