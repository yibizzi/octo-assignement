package ma.octo.assignement.exceptions.Versements;

import ma.octo.assignement.exceptions.TransactionException;

public class MontantMaximalDeVersementAtteintException extends TransactionException {
    public MontantMaximalDeVersementAtteintException() {
        super("Montant maximal de versement dépassé");
    }
}
