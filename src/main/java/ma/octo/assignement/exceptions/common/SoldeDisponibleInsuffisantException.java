package ma.octo.assignement.exceptions.common;

import ma.octo.assignement.exceptions.TransactionException;

public class SoldeDisponibleInsuffisantException extends TransactionException {

  private static final long serialVersionUID = 1L;

  public SoldeDisponibleInsuffisantException() {
  }

  public SoldeDisponibleInsuffisantException(String message) {
    super(message);
  }
}
