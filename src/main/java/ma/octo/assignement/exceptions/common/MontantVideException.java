package ma.octo.assignement.exceptions.common;

import ma.octo.assignement.exceptions.TransactionException;

public class MontantVideException extends TransactionException {
    public MontantVideException() {
        super("Montant Vide");
    }
}