package ma.octo.assignement.exceptions.common;

import ma.octo.assignement.exceptions.TransactionException;

public class CompteNonExistantException extends TransactionException {

  private static final long serialVersionUID = 1L;

  public CompteNonExistantException(String nmrDeCompte) {
    super("Compte Non existant: " + nmrDeCompte);
  }

}
