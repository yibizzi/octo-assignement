package ma.octo.assignement.validation;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.Versements.MontantMaximalDeVersementAtteintException;
import ma.octo.assignement.exceptions.Versements.MontantMinimalDeVersementNonAtteintException;
import ma.octo.assignement.exceptions.Virements.MontantMaximalDeVirementAtteintException;
import ma.octo.assignement.exceptions.Virements.MontantMinimalDeVirementNonAtteintException;
import ma.octo.assignement.exceptions.common.CompteNonExistantException;
import ma.octo.assignement.exceptions.common.MontantVideException;
import ma.octo.assignement.service.ComptesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersementValidator implements TransactionValidator<VersementDto>{


    public static final int MONTANT_DE_VERSEMENT_MAXIMAL = 10000;
    public static final int MONTANT_DE_VERSEMENT_MINIMAL = 10;

    @Autowired
    ComptesService comptesService;

    @Override
    public void validateTransactionData(VersementDto versementDto) throws TransactionException {
        if (versementDto.getMontantVersement() == null || versementDto.getMontantVersement().intValue() == 0) {
            throw new MontantVideException();
        } else if (versementDto.getMontantVersement().intValue() < MONTANT_DE_VERSEMENT_MINIMAL) {
            throw new MontantMinimalDeVersementNonAtteintException();
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_DE_VERSEMENT_MAXIMAL) {
            throw new MontantMaximalDeVersementAtteintException();
        }
        if (versementDto.getMotif() == null || versementDto.getMotif().length() <= 0) {
            throw new TransactionException("Motif vide");
        }

        if(!comptesService.isValidAccount(versementDto.getNrCompteBeneficiaire())){
            throw new CompteNonExistantException(versementDto.getNrCompteBeneficiaire());
        }
    }
}
