package ma.octo.assignement.validation;

import ma.octo.assignement.exceptions.TransactionException;

public interface TransactionValidator<T> {
    public void validateTransactionData(T transaction) throws TransactionException;
}
