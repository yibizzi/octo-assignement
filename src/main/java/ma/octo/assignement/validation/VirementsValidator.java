package ma.octo.assignement.validation;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.common.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.Virements.MontantMaximalDeVirementAtteintException;
import ma.octo.assignement.exceptions.Virements.MontantMinimalDeVirementNonAtteintException;
import ma.octo.assignement.exceptions.common.MontantVideException;
import ma.octo.assignement.service.ComptesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VirementsValidator implements TransactionValidator<VirementDto>{

    public static final int MONTANT_DE_VIREMENT_MAXIMAL = 10000;
    public static final int MONTANT_DE_VIREMENT_MINIMAL = 10;

    @Autowired
    ComptesService comptesService;

    /**
     * Vérifier si le virement envoyé contient des données valides.
     * @param virementDto
     * @throws TransactionException
     */
    @Override
    public void validateTransactionData(VirementDto virementDto) throws TransactionException{
        if (virementDto.getMontantVirement() == null || virementDto.getMontantVirement().intValue() == 0) {
            throw new MontantVideException();
        } else if (virementDto.getMontantVirement().intValue() < MONTANT_DE_VIREMENT_MINIMAL) {
            throw new MontantMinimalDeVirementNonAtteintException();
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_DE_VIREMENT_MAXIMAL) {
            throw new MontantMaximalDeVirementAtteintException();
        }
        if (virementDto.getMotif() == null || virementDto.getMotif().length() <= 0) {
            throw new TransactionException("Motif vide");
        }
        if(!comptesService.isValidAccount(virementDto.getNrCompteEmetteur())){
            throw new CompteNonExistantException(virementDto.getNrCompteEmetteur());
        }
        if(!comptesService.isValidAccount(virementDto.getNrCompteBeneficiaire())){
            throw new CompteNonExistantException(virementDto.getNrCompteBeneficiaire());
        }
    }
}
