package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ComptesController {


    @Autowired
    private CompteRepository compteRepository;

    @GetMapping("/comptes")
    List<Compte> loadAllComptes() {
        List<Compte> compteList = compteRepository.findAll();

        if (CollectionUtils.isEmpty(compteList)) {
            return null;
        } else {
            return compteList;
        }
    }
}
