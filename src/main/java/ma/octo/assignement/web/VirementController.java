package ma.octo.assignement.web;

import ma.octo.assignement.domain.Virement;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.common.CompteNonExistantException;
import ma.octo.assignement.exceptions.common.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.validation.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/virements")
class VirementController {

    @Autowired
    VirementService virementService;

    @Autowired
    private VirementMapper virementMapper;

    @Autowired
    private TransactionValidator<VirementDto> virementsValidator;


    @GetMapping("/virements")
    List<Virement> loadAllVirements() {
        return virementService.getListeDesVirements();
    }

    @PostMapping("/virements")
    @ResponseStatus(HttpStatus.CREATED)
    public VirementDto createVirement(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        /**
         * Vérifier si le virement envoyé est valide.
         */
        virementsValidator.validateTransactionData(virementDto);

        /**
         * Convertir l'objet VirmentDTO en Virment, puis Effectuer le virement.
         */
        virementService.effectuerVirement(this.virementMapper.mapVirementDtoToVirement(virementDto));

        return virementDto;
    }
}
