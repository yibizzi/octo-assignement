package ma.octo.assignement.web;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuditsController {
    @Autowired
    AuditService auditService;

    @GetMapping("/audits")
    public List<Audit> getListeDesAudits(){
        return auditService.getListeDesAudits();
    }
}
