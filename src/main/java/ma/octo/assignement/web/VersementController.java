package ma.octo.assignement.web;


import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.common.CompteNonExistantException;
import ma.octo.assignement.exceptions.common.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.validation.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/versements")
public class VersementController {
    @Autowired
    VersementService versementService;

    @Autowired
    private VersementMapper versementMapper;

    @Autowired
    private TransactionValidator<VersementDto> versementValidator;


    @GetMapping("/versements")
    List<Versement> loadAllVersementss() {
        return versementService.getListeDesVersements();
    }

    @PostMapping("/versements")
    @ResponseStatus(HttpStatus.CREATED)
    public VersementDto createVersement(@RequestBody VersementDto versementDto)
            throws TransactionException {
        /**
         * Vérifier si le versement envoyé est valide.
         */
        versementValidator.validateTransactionData(versementDto);

        /**
         * Convertir l'objet VersementDTO en Versement, puis Effectuer le versement.
         */
        versementService.effectuerVersement(this.versementMapper.mapVersementDtoToVersement(versementDto));

        return versementDto;
    }
}
