package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.UtilisateursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UtilisateursController {

    @Autowired
    private UtilisateursService utilisateursService;


    @GetMapping("/utilisateurs")
    List<Utilisateur> getListeDesUtilisateurs() {
        return utilisateursService.getListeDesUtilisateurs();
    }
}
