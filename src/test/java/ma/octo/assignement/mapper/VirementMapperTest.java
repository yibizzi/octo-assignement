package ma.octo.assignement.mapper;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class VirementMapperTest {

    @Autowired
    VirementRepository virementRepository;

    @Autowired
    VirementMapper virementMapper;

    @Test
    void shouldConvertVirementToVirementDto(){
        Virement exampleVirement = virementRepository.findAll().get(0);


        VirementDto virementDto = this.virementMapper.mapVirementToVirementDto(exampleVirement);

        assertThat(virementDto.getNrCompteEmetteur()).isEqualTo(exampleVirement.getCompteEmetteur().getNrCompte());
        assertThat(virementDto.getNrCompteBeneficiaire()).isEqualTo(exampleVirement.getCompteBeneficiaire().getNrCompte());
        assertThat(virementDto.getMontantVirement()).isEqualTo(exampleVirement.getMontantVirement());
        assertThat(virementDto.getDate()).isEqualTo(exampleVirement.getDateExecution());
        assertThat(virementDto.getMotif()).isEqualTo(exampleVirement.getMotifVirement());

    }

    @Test
    void shouldConvertVirementDtoToVirement(){


        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(BigDecimal.valueOf(150));
        virementDto.setNrCompteBeneficiaire("010000B025001000");
        virementDto.setNrCompteEmetteur("010000A000001000");
        virementDto.setDate(new Date());
        virementDto.setMotif("Test mapping");

        Virement virement =  this.virementMapper.mapVirementDtoToVirement(virementDto);

        assertThat(virementDto.getNrCompteEmetteur()).isEqualTo(virement.getCompteEmetteur().getNrCompte());
        assertThat(virementDto.getNrCompteBeneficiaire()).isEqualTo(virement.getCompteBeneficiaire().getNrCompte());
        assertThat(virementDto.getMontantVirement()).isEqualTo(virement.getMontantVirement());
        assertThat(virementDto.getDate()).isEqualTo(virement.getDateExecution());
        assertThat(virementDto.getMotif()).isEqualTo(virement.getMotifVirement());

    }
}
