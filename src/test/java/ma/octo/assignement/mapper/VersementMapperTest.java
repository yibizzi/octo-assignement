package ma.octo.assignement.mapper;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class VersementMapperTest {

    @Autowired
    VersementMapper versementMapper;

    @Test
    void shouldConvertVersementToVersementDto(){
        // Create Test Data

        Versement exampleVersement = new Versement();
        Compte testCompte = new Compte();
        testCompte.setNrCompte("010000B025001000");

        exampleVersement.setMontantVersement(BigDecimal.valueOf(150));
        exampleVersement.setCompteBeneficiaire(testCompte);
        exampleVersement.setNom_prenom_emetteur("Test Deposit User");
        exampleVersement.setDateExecution(new Date());
        exampleVersement.setMotifVersement("Test mapping");


        // Test Mapping
        VersementDto virementDto = this.versementMapper.mapVersementToVersementDto(exampleVersement);

        assertThat(virementDto.getNomCompletEmetteur()).isEqualTo(exampleVersement.getNom_prenom_emetteur());
        assertThat(virementDto.getNrCompteBeneficiaire()).isEqualTo(exampleVersement.getCompteBeneficiaire().getNrCompte());
        assertThat(virementDto.getMontantVersement()).isEqualTo(exampleVersement.getMontantVersement());
        assertThat(virementDto.getDate()).isEqualTo(exampleVersement.getDateExecution());
        assertThat(virementDto.getMotif()).isEqualTo(exampleVersement.getMotifVersement());

    }

    @Test
    void shouldConvertVersementDtoToVersement(){

        //Create Test Data
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(BigDecimal.valueOf(150));
        versementDto.setNrCompteBeneficiaire("010000B025001000");
        versementDto.setNomCompletEmetteur("Test Deposit User");
        versementDto.setDate(new Date());
        versementDto.setMotif("Test mapping");

        //Test Mapping
        Versement versement =  this.versementMapper.mapVersementDtoToVersement(versementDto);

        assertThat(versementDto.getNomCompletEmetteur()).isEqualTo(versement.getNom_prenom_emetteur());
        assertThat(versementDto.getNrCompteBeneficiaire()).isEqualTo(versement.getCompteBeneficiaire().getNrCompte());
        assertThat(versementDto.getMontantVersement()).isEqualTo(versement.getMontantVersement());
        assertThat(versementDto.getDate()).isEqualTo(versement.getDateExecution());
        assertThat(versementDto.getMotif()).isEqualTo(versement.getMotifVersement());

    }
}
