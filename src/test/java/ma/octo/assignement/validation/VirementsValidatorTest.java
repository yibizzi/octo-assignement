package ma.octo.assignement.validation;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.TransactionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VirementsValidatorTest {

    VirementDto exampleVirementDto;


    @Autowired
    VirementsValidator virementsValidator;

    @BeforeEach
    public void createVirementExample(){
        exampleVirementDto = new VirementDto(
                "exampleEmetteur",
                "exempleBnificiaire",
                "motif",
                new BigDecimal("500"),
                new Date()
        );

    }

    @Test
    void shoudNotAcceptVirementsWithNullMontant() {
        exampleVirementDto.setMontantVirement(null);
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }

    @Test
    void shoudNotAcceptVirementsWithZeroMontant() {

        exampleVirementDto.setMontantVirement(new BigDecimal(0));
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }

    @Test
    void shoudNotAcceptVirementsLowerThanMinimum() {

        exampleVirementDto.setMontantVirement(new BigDecimal(virementsValidator.MONTANT_DE_VIREMENT_MINIMAL*0.1));
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }

    @Test
    void shoudNotAcceptVirementsOverMaximum() {
        exampleVirementDto.setMontantVirement(new BigDecimal(virementsValidator.MONTANT_DE_VIREMENT_MAXIMAL*2));
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }

    @Test
    void shoudNotAcceptVirementsWithoutMotif() {
        exampleVirementDto.setMotif(null);
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }
    @Test
    void shoudNotAcceptVirementsWithEmptyMotif() {
        exampleVirementDto.setMotif("");
        assertThrows(
                TransactionException.class,
                ()-> virementsValidator.validateTransactionData(exampleVirementDto)
        );
    }

}
